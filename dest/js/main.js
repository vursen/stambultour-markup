$(document).ready(function() {

  $.fancybox.defaults.padding = 0;

  $('[data-book-form]').click(function(e) {
    $.fancybox($('.b-book-form_modal'), {
      closeBtn: false
    });

    e.preventDefault();
  });

  /* Стилизация селектов
  ================================================================================= */

  $('.js-form-select').styler();

  /*$.fancybox($('.b-book-form_modal'), {
    closeBtn: false
  });*/

  /* Меню в сайдбаре
  ================================================================================= */

  var $headerMenu = $('.b-header__menu');

  if (document.location.pathname.match(/\/([^\/]+)$/)) {
    $headerMenu
      .find('.b-header__menu__item a[href$="' + document.location.pathname.match(/\/([^\/]+)$/)[1] + '"]')
      .parent()
      .addClass('_active');
  }

  /* Слайдеры
  ================================================================================= */

  var sliderParams = {
    controls: false, 
    pager: false, 
    auto: true,
    mode: 'fade'
  }

  $('.b-photo-slider__slides').bxSlider(sliderParams);

  $('.b-tour-slider__slides').bxSlider(sliderParams);

  /* Туры в сайдбаре
  ================================================================================= */

  var $sidebarTour = $('.b-sidebar-tour');

  $sidebarTour
    .filter('._active')
    .find('.b-sidebar-tour__info')
    .show();

  $sidebarTour.find('.b-sidebar-tour__toggle').click(function(e) {
    var $tour = $(this).closest('.b-sidebar-tour');

    if ($tour.hasClass('_active')) return;

    $sidebarTour
      .removeClass('_active')
      .find('.b-sidebar-tour__info')
      .slideUp(500);

    $tour
      .addClass('_active')
      .find('.b-sidebar-tour__info')
      .slideDown(500);

    e.preventDefault();
  });

  /* Форма бронирования тура
  ================================================================================= */

  var $bookForm = $('.b-book-form');

  $bookForm.find('.b-book-form__time__btn').click(function(e) {
    var $this = $(this);

    $bookForm
      .find('.b-book-form__time__item')
      .removeClass('_checked')
      .find('input[type="radio"]')
      .attr('checked', false);
    
    $this
      .closest('.b-book-form__time__item')
      .addClass('_checked')
      .find('input[type="radio"]')
      .attr('checked', true);

    e.preventDefault();
  })

});