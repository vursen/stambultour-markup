/* Require node modules
-------------------------------------------------------------------------------------- */

var gulp           = require('gulp');
var $              = require('gulp-load-plugins')();
var fs             = require('fs');
var dateFormat     = require('dateformat');
var koutoSwiss     = require('kouto-swiss');
var path           = require('path');

/* Compile Stylus files
-------------------------------------------------------------------------------------- */

gulp.task('stylus', function() {

    gulp.src(['src/stylus/main.styl', 'src/stylus/ie.styl'])
        .pipe($.stylus({
            use: [koutoSwiss()]
        }))
        .on('error', onError)
        .pipe($.autoprefixer({
            browsers: ['Firefox > 20', 'Chrome > 20', '> 1%', 'IE >= 8'],
            cascade: false
        }))
        .on('error', onError)
        .pipe(gulp.dest('dest/css/'));
        /*.pipe(rename({suffix: '.min'}))
        .pipe($.minifycss())
        .pipe(gulp.dest('dest/css/'));*/

});

/* Bower
-------------------------------------------------------------------------------------- */

gulp.task('bower', function() {

    gulp.src(mainBowerFiles({ 
            base: 'src/libs' 
        }))
        .pipe(gulp.dest('dest/libs/'));

});

/* Concat and minify scripts
-------------------------------------------------------------------------------------- */

gulp.task('scripts', function() {

    // gulp.src(['src/js/**/*.js']) //, '!./src/js/vendor/**/*.js'
    //     //.pipe(concat('main.js'))
    //     .on('error', onError)
    //     .pipe(gulp.dest('dest/js'));

});

/* Compile Jade files
-------------------------------------------------------------------------------------- */

gulp.task('jade', function() {

    gulp.src(['src/jade/*.jade'])
        .pipe($.jade({
            pretty: true
        }))
        .on('error', onError)
        .pipe(gulp.dest('dest/'));

});

/* Optimize images
-------------------------------------------------------------------------------------- */

gulp.task('images', function() {

    /*gulp.src(['src/img/*.png','src/img/*.jpg','src/img/*.gif','src/img/*.jpeg'])
        .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
        .pipe(gulp.dest('dest/img'));*/

});

/* Start server
-------------------------------------------------------------------------------------- */

gulp.task('server', function() {

    gulp.src('dest')
        .pipe($.webserver({
            livereload: true,
            open: false,
            port: 7000
        }))
        .on('error', onError);

});

/* Create ZIP archive of `dist` folder
-------------------------------------------------------------------------------------- */

gulp.task('release', function() {

    gulp.src(['dest/**/*', '!.git/**/*'])
        .pipe($.zip([path.basename(__dirname), 'release', dateFormat(new Date(), 'dd.mm.yyyy'), 'zip'].join('.')))
        .on('error', onError)
        .pipe(gulp.dest('./'));

});

/* Create ZIP archive of all project resources
-------------------------------------------------------------------------------------- */

gulp.task('backup', function() {

    gulp.src(['./**/*', '!node_modules/**/*'])
        .pipe($.zip([path.basename(__dirname), 'backup', dateFormat(new Date(), 'dd.mm.yyyy'), 'zip'].join('.')))
        .on('error', onError)
        .pipe(gulp.dest('./'));

});

/* Watch
-------------------------------------------------------------------------------------- */

gulp.task('watch', ['server', 'stylus', 'jade'], function() {

    gulp.watch('src/stylus/**/*', ['stylus']);

    gulp.watch('src/jade/**/*', ['jade']);

});

/* Create
-------------------------------------------------------------------------------------- */

gulp.task('create', function() {

    if ($.util.env.blocks) {
        $.util.env.blocks.split(',').forEach(function(name) {
            fs.writeFile(['src/stylus/blocks/b-', name, '.styl'].join(''), '');
        });
    }

    if ($.util.env.partials) {
        $.util.env.partials.split(',').forEach(function(name) {
            fs.writeFile(['src/jade/partials/', name, '.jade'].join(''), '');
        });
    }

});


/* Clear
-------------------------------------------------------------------------------------- */

gulp.task('clear', function (done) {

    return $.cache.clearAll(done);

});

/* Deploy
-------------------------------------------------------------------------------------- */

gulp.task('deploy', function(cb) {

    exec('git subtree push --prefix dest origin gh-pages', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    }); 

});

/* Error handling
-------------------------------------------------------------------------------------- */

var onError = function(error) {

    $.util.beep();
    if (error.lineno) console.log('Line №:' + error.lineno);
    if (error.filename) console.log('File:' + error.filename);

    console.log('Message:' + error.message);

}